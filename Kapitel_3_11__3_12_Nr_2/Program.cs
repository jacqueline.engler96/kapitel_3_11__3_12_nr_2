﻿using System;

namespace Kapitel_3_11__3_12_Nr_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int iZahl = 0;
            int iZwischenergebnis = 0;
            int iRest = 0;
            string strAusgabeBinärzahl = "";
            string strEingabe = "";
            bool bFalscheingabe = true;


            do
            {
                Console.WriteLine("Bitte eine ganze Zahl eingeben!");
                strEingabe = Console.ReadLine();
                if (Int32.TryParse(strEingabe, out iZahl))
                {
                    bFalscheingabe = false;
                }

            } while (bFalscheingabe);


            do 
            {
                iZwischenergebnis = iZahl / 2;
                iRest = iZahl % 2;
                strAusgabeBinärzahl = Convert.ToString( iRest ) + strAusgabeBinärzahl;
                iZahl = iZwischenergebnis;
            } while ( iZwischenergebnis > 0);


            Console.WriteLine("Die Dezimalzahl {0} ist die Binärzahl {1}", strEingabe, strAusgabeBinärzahl) ;
            Console.ReadKey();
        }
    }
}
